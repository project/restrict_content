<?php

class restrict_content_handler_field_body extends views_handler_field {
  /**
   * Constructor; calls to base object constructor.
   */
  function construct() {
    parent::construct();

    $this->format = $this->definition['format'];

    $this->additional_fields = array();
    if (!is_numeric($this->format)) {
      $this->additional_fields['format'] = array('field' => $this->format);
    }
  }

  function render($values) {  
    $value = $values->{$this->field_alias};
    $format = is_numeric($this->format) ? $this->format : $values->{$this->aliases['format']};        
    
    // Make sure node ID has been loaded.
    if (!isset($values->nid) && $values->cid) {
      $values->nid = db_result(db_query("SELECT nid FROM {comments} WHERE cid = %d", $values->cid));
    }
    
    // Make sure user ID has been loaded.
    if (!isset($values->uid)) {
      $values->uid = db_result(db_query("SELECT uid FROM {node} WHERE nid = %d", $values->nid));
    }
    
    if (restrict_content_check($values)) {
      if ($value) {
        return check_markup($value, $format, FALSE);
      }
    }
    else {
      return theme('restrict_content_links', $values);
    }
  }

  function element_type() {
    if (isset($this->definition['element type'])) {
      return $this->definition['element type'];
    }

    return 'div';
  }
}
<?php

/**
 * Implementation of hook_views_handlers().
 */
function restrict_content_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'restrict_content') .'/includes',
    ),
    'handlers' => array(
      'restrict_content_handler_field_body' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}